#include <iostream>
#include <string>

int main()
{
	std::string SomeString = "All your base are belong to us!";

	std::cout << "String: " << SomeString << "\n";
	std::cout << "String length: " << SomeString.length() << "\n";
	std::cout << "The first character of the string: " << SomeString.front() << "\n";
	std::cout << "The last character of the string: " << SomeString.back() << "\n";

	return 0;
}